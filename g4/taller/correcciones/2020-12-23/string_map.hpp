template <typename T>
string_map<T>::string_map() : raiz(NULL), _size(0) {
    // COMPLETAR
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    // COMPLETAR
    destruir(raiz);
    _size = 0;
    for(int i = 0; i < d.dicc2.size(); i++) {
        insert(d.dicc2[i]);
        _size++;
    }
}

template <typename T>
vector<pair<string, T>> string_map<T>::dicc() {
    return dicc2;
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
}

template <typename T>
void string_map<T>::destruir(Nodo* n) {
    if(n != NULL) {
        for(int i = 0;i < 256; i++) {
            Nodo* a = (n->siguientes)[i];
            destruir(a);
        }
    delete(n);
    }
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& a) {
    if(raiz == NULL) {
        Nodo *nodo = new Nodo;
        raiz = nodo;
    }
    Nodo *node = raiz;
    for (int i = 0; i < a.first.size(); i++) {
        for (int j = 0; j < 256; j++) {
            if (j == int(a.first[i])) {
                if(node->siguientes[j] == NULL){
                        Nodo *aux = new Nodo;
                        node->siguientes[j] = aux;
                }
                node = node->siguientes[j];
                j = 256;
            }
        }
    }
    _size++;
    node->definicion = a.second;
    node->status = true;
    dicc2.push_back(a);
}

template <typename T>
T& string_map<T>::operator[](const string& key){
    // COMPLETAR
    Nodo* node = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                node = node->siguientes[j];
                j = 256;
            }
        }
    }
    return node->definicion;
}


template <typename T>
int string_map<T>::count(const string& key) const{
    // COMPLETAR
    Nodo* node = raiz;
    if(_size == 0)
        return 0;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                if(node != NULL)
                    node = node->siguientes[j];
                j = 256;
            }
        }
    }
    if(node != NULL) {
        if (node->status) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

template <typename T>
const T& string_map<T>::at(const string& key) const {
    // COMPLETAR
    Nodo* node = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                node = node->siguientes[j];
                j = 256;
            }
        }
    }
    return node->definicion;
}

template <typename T>
T& string_map<T>::at(const string& key) {
    // COMPLETAR
    Nodo* node = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                node = node->siguientes[j];
                j = 256;
            }
        }
    }
    return node->definicion;
}


template <typename T>
void string_map<T>::erase(const string& key) {
    // COMPLETAR
    vector<pair<string, T>> diccaux;
    for(int i = 0; i < dicc2.size(); i++) {
        if(dicc2[i].first != key)
            diccaux.push_back(dicc2[i]);
    }
    dicc2 = diccaux;
    Nodo* node = raiz;
    Nodo* aux = raiz;
    int last = 0;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if(hijos(node) || node->status || node == raiz) {
                aux = node;
                if (j == int(key[i]))
                    last = j;
            }
            if (j == int(key[i])) {
                node = node->siguientes[j];
                j = 256;
            }
        }
    }
    _size--;
    node->status = false;
    Nodo* aDestruir = aux->siguientes[last];
    if (!hijo(node)) {
        destruir(aDestruir);
        aux->siguientes[last] = nullptr;
    }
}


template <typename T>
bool string_map<T>::hijos(Nodo const* n) {
    int acum = 0;
    for (int i = 0; i < 256; i++) {
        if (n->siguientes[i] != NULL)
            acum++;
    }
    if (acum > 1)
        return true;
    return false;
}

template <typename T>
bool string_map<T>::hijo(Nodo const* n) {
    for (int i = 0; i < 256; i++) {
        if (n->siguientes[i] != NULL)
            return true;
    }
    return false;
}

template <typename T>
int string_map<T>::size() const{
    // COMPLETAR
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    // COMPLETAR
    return _size == 0;

}