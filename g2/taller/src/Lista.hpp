#include "Lista.h"

Lista::Lista() : length(0), first(NULL), last(NULL) {}
    // Completar

Lista::Lista(const Lista& l) : length(0), first(NULL), last(NULL) {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    borrar();
}

void Lista::borrar() {
    Nodo* temp = first;
    while(temp != NULL) {
        temp = temp->next;
        delete(first->data);
        delete(first);
        first = temp;
    }
    length = 0;
    last = NULL;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    borrar();
    for(int j = 0; j < aCopiar.longitud(); j++) {
        agregarAtras(aCopiar.iesimo(j));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    Nodo* node = new Nodo(); //node: nodo a agregar
    node->data = new int(elem);
    node->next = first;
    node->prev = NULL;
    if(first != NULL) {
        first->prev = node;
    } else {
        last = node;
    }
    first = node;
    this->length++;
}

void Lista::agregarAtras(const int& elem) {
    // Completar
    Nodo* node = new Nodo(); //node: nodo a agregar
    node->data = new int(elem);
    node->next = NULL;
    node->prev = NULL;
    if(last != NULL) {
        node->prev = last;
        last->next = node;
    } else {
        first = node;
    }
    last = node;
    this->length++;
}

void Lista::eliminar(Nat i) {
    // Completar
    Nodo* a_eliminar = first;
    for(int j = 0; j < length; j++) {
        if ( j == i) {
            if(a_eliminar->prev != NULL) {
                a_eliminar->prev->next = a_eliminar->next;
            } else { //Estoy en el primero
                if(a_eliminar->next != NULL)
                    a_eliminar->next->prev = NULL;
                first = a_eliminar->next;
                length--;
                delete(a_eliminar->data);
                delete(a_eliminar);
                return;
            }
            if(a_eliminar->next != NULL) {
                a_eliminar->next->prev = a_eliminar->prev;
            } else { //Estoy en el último
                if(a_eliminar->prev != NULL)
                    a_eliminar->prev->next = NULL;
                last = a_eliminar->prev;
                length--;
                delete(a_eliminar->data);
                delete(a_eliminar);
                return;
            }
            length--;
            delete(a_eliminar->data);
            delete(a_eliminar);
            return;
        }
        a_eliminar = a_eliminar->next;
    }
}

int Lista::longitud() const {
    // Completar
    return length;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* nodo = first;
    for(int j = 0; j < length; j++) {
        if ( j == i) {
            return *nodo->data;
        }
        nodo = nodo->next;
    }
    assert(false);
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* nodo = first;
    for(int j = 0; j < length; j++) {
        if ( j == i) {
            return *nodo->data;
        }
        nodo = nodo->next;
    }
    assert(false);
}

void Lista::mostrar(ostream& o) {
    // Completar
    o << "[";
    Nodo* node = this->first;
    for(int i = 0; i < this->length; i++) {
        o << node;
        node = this->first->next;
    }
    o << "]" << endl;
}
