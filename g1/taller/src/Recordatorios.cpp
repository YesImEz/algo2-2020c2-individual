#include <iostream>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
#endif
private:
    //Completar miembros internos
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia) {}

int Fecha::mes(){
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

void Fecha::incrementar_dia() {
    if(dia_+1>dias_en_mes(mes_)) {
        dia_ = 1;
        if(mes_+1>12) {
            mes_ = 0;
        }
        mes_++;
    } else {
        dia_++;
    }
}

ostream& operator<<(ostream& os, Fecha a) {
    os << a.dia() << "/" << a.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia() && this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia;
}
#endif

// Ejercicio 11, 12

// Clase Horario
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);
private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

ostream& operator<<(ostream& os, Horario a) {
    os << a.hora() << ":" << a.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool iguales = this->hora() == h.hora() && this->min() == h.min();
    return iguales;
}

bool Horario::operator<(Horario h) {
// Completar
    if(this->hora() < h.hora()){
        return true;
    }
    if(this->hora() > h.hora()) {
        return false;
    }
    if (this->min() < h.min()) {
        return true;
    }
    return false;
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h,string s);
    Horario horario_recordatorio();
    Fecha fecha_recordatorio();
    string mensaje_recordatorio();
private:
    Fecha fecha_;
    Horario horario_;
    string mensaje_;
};

Horario Recordatorio::horario_recordatorio(){
    return horario_;
}
Fecha Recordatorio::fecha_recordatorio() {
    return fecha_;
}
string Recordatorio::mensaje_recordatorio() {
    return mensaje_;
}

Recordatorio::Recordatorio(Fecha f, Horario h,string s) : fecha_(f), horario_(h), mensaje_(s) {}

ostream& operator<<(ostream& os, Recordatorio b) {
    os << b.mensaje_recordatorio() << " @ " << b.fecha_recordatorio() << " " << b.horario_recordatorio();
    return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    vector<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    vector<Recordatorio> memory_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial) {} //agenda_() {}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    memory_.push_back(rec);
}

vector<Recordatorio> Agenda::recordatorios_de_hoy() {
    vector<Recordatorio> r;
    for(int i = 0; i < memory_.size(); i++) {
        if(memory_[i].fecha_recordatorio() == hoy_) {
            r.push_back(memory_[i]);
        }
    }
    for(int j = r.size() - 1; j > 0; j--) {
        if(r[j].horario_recordatorio() < r[j - 1].horario_recordatorio()) {
            Recordatorio rec(Recordatorio(r[j].fecha_recordatorio(), r[j].horario_recordatorio(),r[j].mensaje_recordatorio()));
            r[j] = r[j - 1];
            r[j - 1] = rec;
        }
    }
    return r;
}

ostream& operator<<(ostream& os, Agenda ag) {
    ag.recordatorios_de_hoy();
    os << ag.hoy() << endl << "=====" << endl;
    for(int i = 0; i < ag.recordatorios_de_hoy().size(); i++) {
        os << ag.recordatorios_de_hoy()[i] << endl;
    }
    return os;
}

Fecha Agenda::hoy() {
    return hoy_;
}