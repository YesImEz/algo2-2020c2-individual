
template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL), long_(0) {}
    // Completar

template <class T>
Conjunto<T>::~Conjunto() { 
    // Completar
    delete(_raiz);
}

template<class T>
Conjunto<T>::Nodo::~Nodo(){
        delete(izq);
        delete(der);
}

template<class T>
Conjunto<T>::Nodo::Nodo(const T& v) : valor(v), izq(NULL), der(NULL), padre(NULL) {}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if(cardinal() == 0)
        return false;
    Nodo* node = NULL;
    node = _raiz;
    while(node != NULL) {
        if (node->valor != clave) {
            if (node->valor < clave) {
                node = node->der;
            } else {
                if (node->valor > clave) {
                    node = node->izq;
                }
            }
        } else {
            return true;
        }
    }
    delete(node);
    return false;
    assert(false);
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)) {
        if (cardinal() == 0) {
            Nodo* nod = new Nodo(clave);
            _raiz = nod;
            long_++;
            return;
        }
        long_++;
        Nodo* node = NULL;
        node = _raiz;
        while(node != NULL) {
            if (node->valor > clave) {
                if (node->izq == NULL) {
                    node->izq = new Nodo(clave);
                    node->izq->padre = node;
                    return;
                }
                    node = node->izq;
            }
            if (node->valor < clave) {
                if (node->der == NULL) {
                    node->der = new Nodo(clave);
                    node->der->padre = node;
                    return;
                }
                    node = node->der;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(!pertenece(clave))
        return;
    if(_raiz == NULL)
        return;
    if(cardinal() == 1) {
        delete (_raiz);
        _raiz = NULL;
        long_--;
        return;
    }
    Nodo* node = NULL;
    node = _raiz;
    string direc = "";
    while(node->valor != clave) {
            if (node->valor < clave) {
                node = node->der;
                direc = "DER";
            } else {
                node = node->izq;
                direc = "IZQ";
            }

    }
    if (node->valor == clave) {
        if (node->izq == NULL && node->der == NULL) {
            if (direc == "DER")
                node->padre->der = NULL;
            if (direc == "IZQ")
                node->padre->izq = NULL;
            delete (node);
            long_--;
        } else {
            if(node->der != NULL) {
                Nodo* minder = NULL;
                minder = node->der;
                while (minder->izq != NULL) {
                    minder = minder->izq;
                }
                remover(minder->valor);
                node->valor = minder->valor;
            } else {
                Nodo* maxizq = NULL;
                maxizq = node->izq;
                while (maxizq->der != NULL) {
                    maxizq = maxizq->der;
                }
                remover(maxizq->valor);
                node->valor = maxizq->valor;
            }
        }
    }
}


template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    if(!pertenece(clave))
        assert(false);
    Nodo* node = NULL;
    node = _raiz;
    while(node->valor != clave) {
        if (node->valor < clave) {
            node = node->der;
        } else {
            node = node->izq;
        }

    }
    if(node->der != NULL) {
        node = node->der;
        while (node->izq != NULL) {
            node = node->izq;
        }
        return node->valor;
    }
    Nodo* aux = node->padre;
    return aux->valor;
    assert(false);
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* node = NULL;
    node = _raiz;
    while (node->izq != NULL) {
        node = node->izq;
    }
    return node->valor;
    assert(false);
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* node = NULL;
    node = _raiz;
    while (node->der != NULL) {
        node = node->der;
    }
    return node->valor;
    assert(false);
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return long_;
    assert(false);
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

